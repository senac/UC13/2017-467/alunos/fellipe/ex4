package br.com.senac.exercicio4;

import java.util.HashMap;
import java.util.List;

public class FolhaPagamento {

    private HashMap<Integer, Double> comissoes = new HashMap<>();

    public FolhaPagamento() {

    }

    private final double TAXA_COMISSAO = 0.05;

    public void calcularComissao(List<Venda> lista) {

        for (Venda v : lista) {

            int codigo = v.getVendedor().getCodigo();

            if (!comissoes.containsKey(codigo)) {
                comissoes.put(codigo, v.getTotal() * TAXA_COMISSAO);
            } else {
                double comissaoParcial = comissoes.get(codigo).doubleValue();
                comissaoParcial += (v.getTotal() * TAXA_COMISSAO);
                comissoes.put(codigo, comissaoParcial);
            }

        }

    }

    public double getComissaoVendedor(Vendedor vendedor) {
        return this.comissoes.get(vendedor.getCodigo()).doubleValue();
    }

}
