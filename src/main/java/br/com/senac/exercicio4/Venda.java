package br.com.senac.exercicio4;

import java.util.ArrayList;
import java.util.List;

public class Venda {

    private Vendedor vendedor;

    private List<ItemVenda> itens;

    public Venda() {
        this.itens = new ArrayList<>();

    }

    public Venda(Vendedor vendedor) {
        this();
        this.vendedor = vendedor;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public List<ItemVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemVenda> itens) {
        this.itens = itens;
    }

    public void adicionarItem(Produto produto, int quantidade, double valor) {
        this.itens.add(new ItemVenda(produto, quantidade, valor));
    }

    public void removerItem(int posicao) {
        this.itens.remove(posicao);
    }

    public double getTotal() {
        double total = 0;

        for (ItemVenda iten : this.itens) {
            total += iten.getTotal();
        }

        return total;

    }

}
