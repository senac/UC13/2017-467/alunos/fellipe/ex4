package br.com.senac.exercicio4;

public class Vendedor {

    private String nome;
    private int codigo;

    public Vendedor() {
    }

    public Vendedor(String nome, int codigo) {
        this.nome = nome;
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

}
