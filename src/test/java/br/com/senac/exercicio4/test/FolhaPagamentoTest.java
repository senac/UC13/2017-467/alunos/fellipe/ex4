
package br.com.senac.exercicio4.test;

import br.com.senac.exercicio4.FolhaPagamento;
import br.com.senac.exercicio4.Produto;
import br.com.senac.exercicio4.Venda;
import br.com.senac.exercicio4.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class FolhaPagamentoTest {
    
    public FolhaPagamentoTest() {
        
        
    }
    
    @Test
      public void deveCalcularComissaoParaMessi(){
        
        Vendedor vendedor = new Vendedor("Messi", 1);
        Produto produto = new Produto("Gatorade", 1);
        
        
        Venda venda1 = new Venda(vendedor) ;
        venda1.adicionarItem(produto , 1 , 10 ) ;
        
        Venda venda2 = new Venda(vendedor);
        venda2.adicionarItem(produto, 1, 10);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissao = folhaPagamento.getComissaoVendedor(vendedor);

        assertEquals(1, comissao , 0.01);

        
    }

     @Test
    public void deveCalcularComissaoParaMessiECR7(){
        
        Vendedor messi = new Vendedor("messi", 1);
        Vendedor cr7 = new Vendedor("cr7", 2);
        
        Produto produto = new Produto("gatorade", 1);
        
        
        Venda venda1 = new Venda(messi) ;
        venda1.adicionarItem(produto , 10 , 10 ) ;
        
        Venda venda2 = new Venda(cr7);
        venda2.adicionarItem(produto, 10, 20);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissaoMessi = folhaPagamento.getComissaoVendedor(messi) ; 
        
        assertEquals(5.0, comissaoMessi , 0.01);
        
        
        
        
        
        
    }

    
    
}
