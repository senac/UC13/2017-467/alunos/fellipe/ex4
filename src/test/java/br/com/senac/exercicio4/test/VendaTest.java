
package br.com.senac.exercicio4.test;

import br.com.senac.exercicio4.Produto;
import br.com.senac.exercicio4.Venda;
import br.com.senac.exercicio4.Vendedor;
import org.junit.Test;
import static org.junit.Assert.*;


public class VendaTest {
    
    public VendaTest() {
    }
    
    @Test
    public void deveSomaTotalNota(){
        
        Venda venda = new Venda(new Vendedor("Messi", 1));
        Produto produto = new Produto("Gatorade", 1);
        venda.adicionarItem(produto , 1 , 10 ) ; 
        
        double resultado = venda.getTotal() ; 
        
        
        assertEquals(resultado, 10 , 0.05);
        
    }
    

    
    
    
    
}
